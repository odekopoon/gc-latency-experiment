#! /bin/sh

cat "$1.log" | grep "Worst push time:"|xargs -I {} echo {} ms
echo "Longest GC pause (s):"
cat "$1.log" | grep -v concurrent | grep -o "[0-9.]* secs" | sort -n | tail -n 1
echo "Heap size:"
cat "$1.log" | grep -o "InitialHeapSize.*:= [0-9]\+"
cat "$1.log" | grep -o "MaxHeapSize.*:= [0-9]\+"
echo "Execution time (s):"
tail -n 3 "$1.log"
